import { observe, store } from 'shop-packages';
import { setPaymentOption } from 'shop-packages/redux/checkout/actions';
import { selectPending } from 'shop-packages/redux/checkout/selectors';

const trans = {
  error: gettext('An error occured, please try again later.')
};

class PayPal {
  constructor({ pk }) {
    this.form = document.querySelector('.js-paypal-form');
    this.completeButton = this.form.querySelector('.js-checkout-complete-button');
    this.agreementCheckbox = this.form.querySelector('.js-checkout-agreement-input');
    this.agreementError = this.form.querySelector('.js-agreement-error');
    this.errors = this.form.querySelector('.js-form-errors');
    this.observers = [observe(selectPending).subscribe(this.onPending)];

    store.dispatch(setPaymentOption(pk));
    this.bindEvents();
  }

  // Have to use arrow fn's because binding is messed up
  bindEvents = () => {
    this.form.beforeAction = async () => {
      this.errors.innerHTML = '';
      this.errors.setAttribute('hidden', true);
    };

    this.form.addEventListener('action-success', this.onFormSuccess);
    this.form.addEventListener('action-error', this.onFormError);
    this.agreementCheckbox.addEventListener('validation-change', this.onAgreementChange);
  }

  onFormSuccess = (response) => {
    const { data } = response.detail.response;
    const errors = data?.errors;
    const redirectUrl = data?.context_list?.[0]?.page_context?.redirect_url;

    if (errors || !redirectUrl) {
      this.showError();
      return;
    }

    window.location = redirectUrl;
  }

  onFormError = () => {
    this.errors.innerHTML = trans.error;
    this.errors.removeAttribute('hidden');
    this.showError();
  }

  onAgreementChange = (response) => {
    this.agreementError.hidden = response.detail.valid;
  }

  showError = () => {
    this.errors.innerHTML = trans.error;
    this.errors.removeAttribute('hidden');
  }

  onPending = (isPending) => {
    this.completeButton.busy = isPending;
  }

  unmount = () => {
    this.form.removeEventListener('action-success', this.onFormSuccess);
    this.form.removeEventListener('action-error', this.onFormError);
    this.agreementCheckbox.removeEventListener('validation-change', this.onAgreementChange);
    this.observers.forEach((observer) => observer.unsubscribe());
  }
}

export default PayPal;
