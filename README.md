# pz_paypal

This app allows you to use PayPal payment via redirection.

⚠️ Use **legacy** branch for projects without web components / latest pz-core & shop-packages dependencies

## Usage
### 1. Install the app

At project root, create a requirements.txt if it doesn't exist and add the following line to it;

```bash
-e git+https://git@bitbucket.org/akinonteam/pz_paypal.git#egg=pz_paypal
```

Note: If needed, you can add the latest commit id to the above example.
For more information about this syntax, check [pip install docs](https://pip.pypa.io/en/stable/reference/pip_install/#git).

Next, run the following command to install the package.

```bash
# in venv
pip install -r requirements.txt
```

### 2. Install the npm package

```bash
# in /templates
yarn add git+ssh://git@bitbucket.org:akinonteam/pz_paypal.git
```

If you used a commit id in requirements.txt, make sure to use the same git commit id here.

### 3. Add to the project

```python
# omnife_base/settings.py:

INSTALLED_APPS.append('pz_paypal')
```

### 4. Import template

*templates/orders/checkout/payment/index.html*
```jinja
{% from 'pz_paypal/index.html' import PZPaypal %}

<div class="checkout-payment-type__list">
  <!-- ... -->
  <button class="checkout-payment-type__button js-payment-tab" data-type="redirection" hidden>
    {{ _('PayPal') }}
  </button>
</div>

<div class="checkout-payment-type__contents">
  <!-- ... -->
  <div class="js-payment-tab-content" data-type="redirection" hidden>
    {{ PZPaypal() }}
  </div>
</div>
```

### 5. Import JS

*templates/orders/checkout/payment/index.js*
```js
import PayPal from 'pz-paypal';

paymentOptions = new Map(
  [
    // ...
    ['redirection', PayPal]
  ]
);
```

## Customizing the modal
### Default template
You can use the default content area template by calling the macro.

```jinja2
{{ PZPaypal() }}
```

### Custom template

If you want a custom HTML structure, you need to use `call`.

*ATTENTION: When doing this, you must match the appropiate JS selectors from the default template*

```jinja
{% call PZPaypal() %}
  <!-- your HTML -->
{% endcall %}
```

## Template settings

```jinja
{# Defaults #}
{{
  PZPaypal(
    class='',
    title=false,
    button_text=false,
    button_icon='',
    agreement_one='',
    agreement_two='')
}}
```

All of the following are optional parameters for the Jinja2 macro.

- **class**: (String) A general wrapper class for the template

- **title**: (String) Modal title shown on top

- **button_text**: (String) Text to be shown on the payment button

- **button_icon**: (String) Class name to be used for the payment button icon

- **agreement_one**: (String) A proper HTML element used to trigger a PZModal for agreement one

- **agreement_two**: (String) A proper HTML element used to trigger a PZModal for agreement two
